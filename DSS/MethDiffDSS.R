#
# Copyright (C) 2016 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

##################### Analysis of DNA methylation data #########################
################################################################################


### ---------------------Packages
packages <- c("optparse",# to read arguments from a command line
              "ggplot2", # nice plots
              "ggfortify",
              "reshape2",  # reshape grouped data
              "plyr", # tools for Splitting, Applying and Combining Data
              "dplyr", # tools for Splitting, Applying and Combining Data
              "VennDiagram", # for Venn diagram
              "doMC", # for parallel environment
              "grid", #for layout in plots
	            "hexbin", # for correlation plot
              "emdbook",
              "mclust",
              "data.table"

)

for(package in packages){
  # if package is installed locally, load
  if (!(package %in% rownames(installed.packages()))) {
    # install.packages(package, repos="http://cran.univ-paris1.fr")
    install.packages(package)
  }
  do.call('library', list(package))
}

#Bioconductor package
packagesBio <- c("DSS",# for beta-binomial model
              "rtracklayer", # read gff file
              "edgeR", # RLE normalization
              "ChIPpeakAnno", # correspondance gene-DMR
              "methylKit", # for PCAs and clusters plots
              "MIRA" # for BSseq to data.table conversion
)

for(package in packagesBio){
  # if package is installed locally, load
  if (!(package %in% rownames(installed.packages()))) {
    BiocManager::install(package)
  }
  do.call('library', list(package))
}

sessionInfo()
### ---------------------Parameters

#############
###### Parameter initialization when the script is launched from command line
############
option_list = list(
  make_option(c("-d", "--directory"), type = "character", default = NULL,
              help = "name of the directory where all files are stored (files format
              chr    pos    N    X)", metavar = "character"),
  make_option(c("--comp"), type = "character", default = NULL,
              help = "2 groups to compare (ex: 'M,F'). Only if --multifactor is FALSE.",
              metavar = "character"),
  make_option(c("--multifactor"), type = "logical", default = FALSE,
              help = "TRUE if there is another factor in the metadata file. To correct the potential effect of another factor",
	      metavar = "logical"),
  make_option(c("--smoothing"), type = "logical", default = TRUE,
              help = "if TRUE smoothing is on (recommended for WGBS data). Better estimations of mean methylation are obtained due to the spatial correlation [default = %default]\n\t\tshould be provided",
              metavar = "logical"),
  make_option(c("--formula"), type = "character", default = NULL,
              help = "Linear model fitted. Only when --multifactor is TRUE.
                     Exemple of formula: \"tissue+treat\"",
              metavar = "character"),
  make_option(c("--ref"), type = "character", default = NULL,
              help = "The reference level(s) of each factor(s) used for the Wald test.
              Separate each value with a comma (example:\"Sang,TI\")
              Only when --multifactor is TRUE.",
              metavar = "character"),
  make_option(c("--term"), type = "character", default = NULL,
              help = "The effect(s) to be tested.
                      If --multifactor is TRUE and --term is NULL, all effects are tested.",
              metavar = "character"),
  make_option(c("--coef"), type = "character", default = NULL,
              help = "The specific effect(s) to be tested.
                      If --multifactor is TRUE, coefficient in the linear model to be tested.
                      Separate each value with a comma (example:\"TEMOIN,5AZA\").",
              metavar = "character"),
  make_option(c("-m", "--metadata"), type = "character", default = NULL,
              help = "file of summarized data and experience\n\t\tshould be provided",
              metavar = "character"),
  make_option(c("--alpha"), type = "double", default = 0.05,
              help = "significance level of the tests (i.e. acceptable rate of
              false-positive in the list of DMC)
              [default=%default]",
              metavar = "character"),
  make_option(c("--correct"), type = "character", default = "BH",
              help = "method used to adjust p-values for multiple testing
              ('BH' or 'bonferroni') [default=%default]", metavar = "character"),
  make_option(c("--dmr"), type = "logical", default = FALSE,
              help = "if TRUE DMR are extract [default=%default]",
              metavar = "character"),
  make_option(c("--dmr.numC"), type = "double", default = 3,
              help = "cutoff of the number of CpGs (CHH or CHG) in each region to
              call DMR [default=%default]",
              metavar = "character"),
  make_option(c("--dmr.propDMC"), type = "double", default = 1,
              help = "cutoff of the proportion of DMCs in each region to call DMR
              [default=%default]",
              metavar = "character"),
  make_option(c("--dmr.type"), type = "character", default = "qvalue",
              help = "definition of DMC in DMR ('pvalue' or 'qvalue')
              [default=%default]",
              metavar = "character"),
  make_option(c("--min_cov"), type = "integer", default = "5",
              help = "Minimum of coverage depth
              [default=%default]",
              metavar = "character"),
  make_option(c("--gff"), type = "character", default = NULL,
              help = "annotation file (gff ot gtf files)",
              metavar = "character"),
  make_option(c("--type"), type = "character", default = NULL,
              help = "list of interested type of regions separeted by ',' (e.g.
              exon, intron, 5_prime_utr...)",
              metavar = "character"),
  make_option(c("--tss"), type = "character", default = NULL,
              help = "file with TSS (files format: chr    tss    strand)",
              metavar = "character"),
  make_option(c("--parallel"), type = "logical", default = FALSE,
              help = "if TRUE some of the methods are performed on multiple cores
              [default=%default]",
              metavar = "character"),
  make_option(c("--ncores"), type = "integer", default = NULL,
              help = "number of cores in parallel mode",
              metavar = "character"),
  make_option(c("--plots"), type = "logical", default = TRUE,
              help = "if TRUE plots are print
              [default=%default]",
              metavar = "character"),
  make_option(c("--rda"), type = "logical", default = TRUE,
              help = "if TRUE rda are generated
              [default=%default]",
              metavar = "character"),
  make_option(c("-o", "--out"), type = "character", default = NULL,
              help = "folder path where results are stored",
              metavar = "character"),
  make_option(c("--process_functions"), type = "character", default = "/home/pterzian/epigenetique/axis2-epigenectics/script_BSseq/BS_DSS_analysis.R", metavar = "character",
              help = "Complete path to the R functions for preprocessing")
  )

opt_parser = OptionParser(usage="Imports data and find DMC.",
                          option_list = option_list)

opt = parse_args(opt_parser)

# source functions
source(file = opt$process_functions)

#file names are needed to continue
if (is.null(opt$directory)) {
  print_help(opt_parser)
  stop("Files directory missing.\n", call. = FALSE)
}

#results directory is needed to continue
if (is.null(opt$out)) {
  print_help(opt_parser)
  stop("Results directory (--out/-o) missing.\n", call. = FALSE)
}

#metadata is needed to access the bismark files
if (is.null(opt$metadata)){
  print_help(opt_parser)
  stop("Metadata missing (--metadata/-m).\n", call. = FALSE)
}

#formula is needed if multifactor = TRUE
if (opt$multifactor & is.null(opt$formula)){
  print_help(opt_parser)
  stop("Formula of the linear model (--formula) is necessary when multifactor = TRUE.")
}

#term no needed if multifactor = FALSE
if (opt$multifactor==FALSE){
  if (is.null(opt$term) | length(unlist(strsplit(opt$term, "\\+")))!=1){
    print_help(opt_parser)
    stop("Only one effect to be tested (--term) when multifactor = FALSE (ex: 'sex').")
  }
}

#coef can't be used if multifactor = FALSE
if (opt$multifactor==FALSE){
  if (!is.null(opt$coef) ){
    print_help(opt_parser)
    stop("Coef can't be used (--coef) when multifactor = FALSE.")
  }
}

if (opt$multifactor==FALSE){
  if (is.null(opt$comp) | length(unlist(strsplit(opt$comp, ",")))!=2){
    print_help(opt_parser)
    stop("Only 2 groups are needed (--comp) for the comparison when multifactor = FALSE.")
  }
}

if (opt$multifactor & !is.null(opt$comp)){
  print_help(opt_parser)
  stop("--comp can not be used when multifactor = TRUE. Use --term when multifactor = TRUE")
}

#control on correction method
if (!(opt$correct %in% c("BH", "bonferroni"))){
  print_help(opt_parser)
  stop("This normalization method doesn't exist.")
}

#control on correction method
if (opt$plots & !is.null(opt$gff) & is.null(opt$type)){
  print_help(opt_parser)
  stop("type of feature (--type) are necessary when plots = TRUE and gff not NULL.")
}

#control on dmr stats
'%ni%' <- Negate('%in%')
if(opt$dmr.type %ni% c("pvalue","qvalue")){
  print_help(opt_parser)
  stop("The definition of DMC in DMR should be 'pvalue' or 'qvalue'")
}

#Create output folder if it didn't exist
ifelse(!dir.exists(opt$out), dir.create(opt$out), FALSE)

#Initialize parallel environment
if (opt$parallel){
  registerDoMC(opt$ncores)
}

#####################################################
### ---------------------Input data and preprocessing
#####################################################

###########################################
### ---------------------Data preparation
###########################################

#Input normalize data
print("-------------------")
print("Data preparation...")
print("-------------------")

#Read metadata
Metadata <- read.table(opt$metadata, header = TRUE, colClasses="character")

if(opt$multifactor){
  #Vector of sample names
  names <- unlist(levels(as.factor(Metadata[,1])))
}else{
  comps = unlist(strsplit(opt$comp, ","))
  pool1 = as.character(Metadata[Metadata[,opt$term] %in% comps[1],][,1])
  pool2 = as.character(Metadata[Metadata[,opt$term] %in% comps[2],][,1])
  #Vector of sample names
  names=c(pool1,pool2)
  #stop scrit if one pool name doesn't correspond to a filename
  if (sum(!(pool1 %in% names)) != 0 |  sum(!(pool2 %in% names)) != 0){
    stop("A pool name doesn't match to filenames")
  }
  #table with sample names and condition
  sample_info <- data.frame(sample=c(pool1, pool2),
                            condition=c(rep(1,length(pool1)),
                                        rep(2,length(pool2))))
}

#Vector of file names
# bismark_files <- grep(paste(names,collapse="|"), dir(opt$directory), value=TRUE)
bismark_files <- unlist(lapply(names, function(x) {grep(x, dir(opt$directory), value=TRUE)} )) # keep order of samples
#Vector of file paths
bismark_files_path <- normalizePath(file.path(opt$directory, bismark_files), mustWork = FALSE)

print("---------------------")
print("Read bismark files...")
print("---------------------")

files_init = lapply(bismark_files_path, function(x) { x <- fread(x); return(x) } )
files = lapply(files_init, function(x) { x <- x[, c("chr", "pos", "N", "X")]; return(x) } )
strand = lapply(files_init, function(x) { x <- x[, c("chr", "pos", "strand")]; colnames(x)[2] <- "end"; x$chr <- as.factor(x$chr); return(x) } )
mergedf = function(x,y) merge.data.table(x,y,all=TRUE)
strand = Reduce(mergedf,strand)

###########################################
### ---------------------DMC with DSS
###########################################

print("-------------------")
print("Create BSeq object")
print("-------------------")

#create a BSseq object
BSobj <- makeBSseqData(files, names)
BSobj
save(BSobj, file = normalizePath(file.path(opt$out, "BSobj.rda"), mustWork = FALSE))

if(opt$multifactor){
  #Multifactor:
  ##get list of factors from the linear model
  factors = strsplit(opt$formula, "\\+|\\*")[[1]]
  factors = factors[grepl(":", factors)==FALSE]
  ##get list of factors from the ref list
  refs = strsplit(opt$ref, ",")[[1]]
  ##create general experimental design
  design <- cbind(Metadata[factors])
  design[names(design)] <- lapply(design[names(design)], as.factor)

  if(!is.null(opt$ref)){
  ##reorder levels of factors:
    for(i in refs) {
      #get the corresponding column name for each element of the reference list
      col = names(which(apply(design, 2, function(x) any(grepl(paste("^",i,"$", sep = ""), x)))))
      #reorder levels
      levels = levels(factor(design[,col]))
      design[,col] <- relevel(factor(design[,col]), i)
    }
  }

  print("Fit linear model")
  print(paste0("-----formula----- ", opt$formula))
  ##fit linear model for BS-seq data from design
  DMLfit = DMLfit.multiFactor(BSobj, design=design, formula=as.formula(paste("~", opt$formula)), smoothing = opt$smoothing)
  save(DMLfit, file = normalizePath(file.path(opt$out, "DMLfit.rda"), mustWork = FALSE))
  
  print("Perform the test")
  ##perform the test for term
  terms = unlist(strsplit(opt$term, ","))

  print(paste0("-----term----- ", opt$term))
  dmlTest <- lapply(terms, function(i) DMLtest.multiFactor(DMLfit, term = i))
  print(paste0(nrow(dmlTest[[1]])," CpG sites tested"))

  ##perform the test for coef
  if(!is.null(opt$coef)){
    print("Perform the test coef")
    coef_split = strsplit(opt$coef,",")[[1]]
    coef_to_test = colnames(DMLfit$X)[grepl(paste(coef_split, collapse="|"), colnames(DMLfit$X))]
    dmlTest_coef = lapply(coef_to_test, function(x) na.omit(DMLtest.multiFactor(DMLfit, coef=x)))
    print(paste0(nrow(dmlTest_coef[[1]])," CpG sites tested"))
    save(dmlTest_coef, file = normalizePath(file.path(opt$out, "dmlTest_coef.rda"), mustWork = FALSE))
  }

  }else{
  #Single factor:
  ##perform the test
  print(paste0("-----pool1----- ", pool1))
  print(paste0("-----pool2----- ", pool2))
  dmlTest <- DMLtest(BSobj, group1 = pool1, group2 = pool2, smoothing = opt$smoothing)
  print(paste0(nrow(dmlTest)," CpG sites tested"))
  }

save(dmlTest, file = normalizePath(file.path(opt$out, "dmlTest.rda"), mustWork = FALSE))

###########################################
### ---------------------DMC
###########################################

print("-------------------")
print("Get DMC")
print("-------------------")

get_dmc <- function(dml, strand, factor) {
  # Table of p-value: only cytosines with an adjusted p-value < opt$alpha
  dml <- dml[dml$padj < opt$alpha, ]
  # BED format
  dml$start <-dml$pos - 1
  dml$end <-dml$pos
  dml$pos <- NULL
  if(opt$multifactor){
    #Multifactor
   dml <-dml[, c("chr", "start", "end", "pvals", "padj")]
   #merge with strand
   dml$chr <- as.factor(dml$chr)
   res_DMC <- inner_join(dml, strand, by = c("chr", "end"))
   print(paste0("############### Number of DMC (", factor,") : ", nrow(res_DMC)))
  }else{
   #Single factor:
   dml$pool1 <- ifelse(dml$diff > 0, "UP", "DOWN")
   dml <-dml[, c("chr", "start", "end", "mu1", "mu2", "diff", "diff.se",
                           "pval", "padj", "pool1")]
   #merge with strand
   dml$chr <- as.factor(dml$chr)
   res_DMC <- inner_join(dml, strand, by = c("chr", "end"))
   print(paste0("############### Number of DMC : ", nrow(res_DMC)))
  }
  return(res_DMC)
}

write_dmc_txt <- function(dmc,title) {
  #save table
  if(!is.null(dmc) & nrow(dmc) > 0){
    write.table(dmc, file = normalizePath(file.path(opt$out, title), mustWork = FALSE),
                sep = "\t", row.names = FALSE, quote = FALSE)
  }
}

write_dmc_bed <- function(dmc, title, samples) {
  res_DMC_grange <- GRanges(seqnames = dmc$chr, ranges = IRanges(dmc$end, dmc$end), strand = dmc$strand)
  if(opt$multifactor){
    description = paste0("samples (", paste(samples, collapse = ", "), ")")
  }else{
    description = paste0("pool1 (", paste(samples$name[samples$condition == 1], collapse = ", "),
                         ") vs pool2 (", paste(samples$name[samples$condition == 2], collapse = ", "), ")")
  }
  if(!is.null(dmc) & nrow(dmc) > 0){
    export(res_DMC_grange, normalizePath(file.path(opt$out, title), mustWork = FALSE),
           trackLine=new("BasicTrackLine", name = "DMC",
                         description = paste0("samples (", paste(samples, collapse = ", "), ")"),
                         useScore = TRUE))
  }
}

if(opt$multifactor){
  # Multifactor
  #adjust p-value
  dmlTest = lapply(dmlTest, function(x) { x$padj <- p.adjust(x$pvals, method = opt$correct); return(x) } )
  #get dmc
  print("Get res_DMCs...")
  res_DMC <- lapply(1:length(dmlTest), function(i) get_dmc(dml=dmlTest[[i]], strand, terms[i]))
  #save txt and bed tables
  print("Write txt and bed tables...")
  lapply(1:length(res_DMC), function(i) write_dmc_txt(dmc=res_DMC[[i]], paste0("DMC", "_", terms[i], ".txt")))
  lapply(1:length(res_DMC), function(i) write_dmc_bed(dmc=res_DMC[[i]], paste0("DMC", "_", terms[i], ".bed"), names))
  if(!is.null(opt$coef)){
    print("Get res_DMCs coef...")
    #adjust p-value
    dmlTest_coef = lapply(dmlTest_coef, function(x) { x$padj <- p.adjust(x$pvals, method = opt$correct); return(x) } )
    #get dmc
    res_DMC_coef <- lapply(1:length(dmlTest_coef), function(i) get_dmc(dml=dmlTest_coef[[i]], strand, coef_to_test[i]))
    #save txt and bed tables
    print("Write txt and bed tables coef...")
    lapply(1:length(res_DMC_coef), function(i) write_dmc_txt(dmc=res_DMC_coef[[i]], paste0("DMC_", coef_to_test[i], ".txt")))
    lapply(1:length(res_DMC_coef), function(i) write_dmc_bed(dmc=res_DMC_coef[[i]], paste0("DMC_", coef_to_test[i], ".bed"), names))
  }
}else{
  # Single factor
  #adjust p-value
  dmlTest$padj <- p.adjust(dmlTest$pval, method = opt$correct)
  print("Get res_DMCs...")
  res_DMC <- get_dmc(dml=dmlTest, strand, opt$term)
  #save txt and bed tables
  print("Write txt and bed tables...")
  write_dmc_txt(dmc=res_DMC, "DMC.txt")
  write_dmc_bed(dmc=res_DMC, "DMC.bed", sample_info)
}

###########################################
### ---------------------DMR
###########################################

print("-------------------")
print("Get DMR")
print("-------------------")

get_dmr <- function(dml, factor, type=opt$dmr.type){
  if(type=="qvalue"){
    if(opt$multifactor){
      # multifactor
      dml$pvals <- dml$padj
      dml$fdrs <- NULL
    }else{
      # single factor
      dml$pval <- dml$padj
    }
  }
  try(res_DMR <- callDMR(dml, delta = 0, p.threshold = opt$alpha,
                         minCG = opt$dmr.numC, pct.sig = opt$dmr.propDMC),
      silent = TRUE)
  if(!is.null(res_DMR)){
    nb_rows = nrow(res_DMR)
  }else{
    nb_rows = 0
  }
  if(opt$multifactor){
    print(paste0("############### Number of DMR (", factor,") : ", nb_rows))
  }else{
    print(paste0("############### Number of DMR : ", nb_rows))
  }
  return(res_DMR)
}

write_dmr_txt <- function(dmr, title){
  if(!is.null(nrow(dmr))){
    write.table(dmr, file = normalizePath(file.path(opt$out, title),
                                          mustWork = FALSE), sep = "\t",
                row.names = FALSE, quote = FALSE)
  }
}

write_dmr_bed <- function(dmr, title, samples){
  if(opt$multifactor){
    #Multifactor:
    res_DMR_grange <- GRanges(seqnames = dmr$chr, ranges = IRanges(dmr$start, dmr$end))
    if(!is.null(nrow(dmr))){
      export(res_DMR_grange, normalizePath(file.path(opt$out, title), mustWork = FALSE),
             trackLine=new("BasicTrackLine", name = "DMR",
                           description = paste0("samples (", paste(samples, collapse = ", "), ")"),
                           useScore = FALSE))
    }
  }else{
    #Single factor:
    res_DMR_grange <- GRanges(seqnames = dmr$chr, ranges = IRanges(dmr$start, dmr$end),
                              name = dmr$pool1, score = dmr$diff.Methy)
    if(!is.null(nrow(dmr))){
      export(res_DMR_grange, normalizePath(file.path(opt$out, title), mustWork = FALSE),
             trackLine=new("BasicTrackLine", name = "DMR",
                           description = paste0("pool1 (", paste(samples$name[samples$condition == 1], collapse = ", "),
                                                ") vs pool2 (", paste(samples$name[samples$condition == 2], collapse = ", "), ")"),
                           useScore = TRUE))
    }
  }
}

annotate_DMR <- function(dmr,gff,title){
  ## Correspondance DMR-gene
  if(sum(gff@elementMetadata@listData$type == "gene", na.rm = TRUE) > 0 & !is.null(nrow(dmr))){
    res_DMR_grange_annot <- GRanges(seqnames = dmr$chr, ranges = IRanges(dmr$start, dmr$end))

    gene_grange <- GRanges(seqnames = seqnames(gff)[gff@elementMetadata@listData$type == "gene"],
                           IRanges(start(gff)[gff@elementMetadata@listData$type == "gene"],
                                   end = end(gff)[gff@elementMetadata@listData$type == "gene"],
                                   names = gff@elementMetadata@listData$Name[gff@elementMetadata@listData$type == "gene"]))

    #replace NA by increment number
    baseR.replace <- function(x) { replace(names(x), is.na(names(x)), 1:length(x)) }
    names(gene_grange) = baseR.replace(gene_grange)

    annotatedDMR <- suppressWarnings(annotatePeakInBatch(res_DMR_grange_annot,
                                                         AnnotationData=gene_grange,
                                                         output = "both"))
    write.table(as.data.frame(annotatedDMR)[, c("seqnames", "start", "end",
                                                "feature", "start_position",
                                                "end_position", "insideFeature",
                                                "distancetoFeature",
                                                "shortestDistance")],
                file = normalizePath(file.path(opt$out, title),
                                     mustWork = FALSE), sep = "\t",
                row.names = FALSE, quote = FALSE)
  }
}

if(opt$dmr){
  #find DMR
  if(opt$multifactor){
    #get dmr
    print("Get res_DMR...")
    res_DMR = lapply(1:length(dmlTest), function(i) get_dmr(dml=dmlTest[[i]], terms[i], type=opt$dmr.type))
    ##save txt and bed tables
    print("Write txt and bed tables...")
    lapply(1:length(res_DMR), function(i) write_dmr_txt(dmr=res_DMR[[i]], paste0("DMR", "_", terms[i], ".txt")))
    lapply(1:length(res_DMR), function(i) write_dmr_bed(dmr=res_DMR[[i]], paste0("DMR", "_", terms[i], ".bed"), names))
    if(!is.null(opt$coef)){
      print("Get res_DMR coef...")
      res_DMR_coef <-lapply(1:length(dmlTest_coef),  function(i) get_dmr(dml=dmlTest_coef[[i]], coef_to_test[i], type=opt$dmr.type))
      print("Write txt and bed tables coef...")
      lapply(1:length(res_DMR_coef), function(i) write_dmr_txt(dmr=res_DMR_coef[[i]], paste0("DMR_", coef_to_test[i], ".txt")))
      lapply(1:length(res_DMR_coef), function(i) write_dmr_bed(dmr=res_DMR_coef[[i]], paste0("DMR_", coef_to_test[i], ".bed"), names))
    }
  }else{
    #Single factor:
    #get dmr
    print("Get res_DMR...")
    res_DMR = get_dmr(dml=dmlTest, opt$term, type=opt$dmr.type)
    ##save txt and bed tables
    res_DMR$pool1 <- ifelse(res_DMR$diff.Methy > 0, "UP", "DOWN")
    #save txt and bed tables
    print("Write txt and bed tables...")
    write_dmr_txt(dmr=res_DMR, "DMR.txt")
    write_dmr_bed(dmr=res_DMR, "DMR.bed", sample_info)
  }

  if(!is.null(opt$gff)){
    #gff file
    gff <- import.gff3(opt$gff)
    ##write gff table
    print("Write gff table...")
    if(opt$multifactor){
      # annotate_DMR(res_DMR, gff, paste(opt$term, "DMRannoted.txt", sep="_"))
      lapply(1:length(res_DMR), function(i) annotate_DMR(res_DMR[[i]], gff, paste0("DMRannoted", "_", terms[i], ".txt")))
    }else{
      annotate_DMR(res_DMR, gff, "DMRannoted.txt")
    }
  }
}

#####################################################
### -------------------------------------PLOTS
#####################################################

print("-------------------")
print("Plots")
print("-------------------")

if(opt$plots){
  pdf(file.path(opt$out, "images_DMC.pdf"), title = "Plots from DSS (DMC and DMR)",
      width = 12, height = 10)

  print("Plot diff BSobj...")

  #Distribution of the distance between 2 cytosines
  diff <- unlist(sapply(unique(as.character(seqnames(BSobj))),
                        function(x) diff(as.numeric(start(BSobj))[as.character(seqnames(BSobj)) == x[1]])))

  p<-ggplot(data.frame(diff=diff), aes(x = log2(diff))) +
    geom_density(adjust = 2) + ylab("Density") +
    theme_bw() + xlab(label = "Distance between 2 cytosines (log2)") + ggtitle("Distribution of the distance between 2 cytosines")
  print(p)

  get_diff_DMC <- function(dmc){
    diffDMC <- NULL
    if(nrow(dmc) > 0){
      dmc <- dmc[order(dmc$chr, dmc$start), ]
      diffDMC <- unlist(sapply(as.character(unique(dmc$chr)), function(x) diff(dmc$start[dmc == x[1]])))
    }
    return(diffDMC)
  }

  plot_diff_DMC <- function(diffDMC, title){
    #plot
    if(!is.null(diffDMC)){
      p<-ggplot(data.frame(diffDMC=diffDMC), aes(x = log2(diffDMC))) +
        geom_density() + ylab("Density") +
        theme_bw() + xlab(label = "Distance between 2 DMC (log2)") + ggtitle(title)
      print(p)
    }
  }

  print("Plot diff res_DMCs...")
  if(opt$multifactor){
    diffDMC = lapply(1:length(res_DMC), function(i) get_diff_DMC(res_DMC[[i]]))
    lapply(1:length(diffDMC), function(i) plot_diff_DMC(diffDMC[[i]], paste("Distribution of the distance between 2 DMC: ", terms[i], sep="")))
  }else{
    diffDMC = get_diff_DMC(res_DMC)
    plot_diff_DMC(diffDMC, "Distribution of the distance between 2 DMC")
  }

  ####### annot
  dmc_type <- NULL
  prop_by_type <- NULL
  if(!is.null(opt$gff)){
    ## gff file
    gff <- import.gff3(opt$gff)

    #table with only chr-start-end-type from gff
    annot <- data.frame(chr = seqnames(gff), start = start(gff), end = end(gff),
                        strand = strand(gff),
                        type = gff@elementMetadata@listData$type)

    #type of interest
    type <- unlist(strsplit(opt$type, ','))

    #annotation with only type of interest
    annot <- annot[annot$type %in% type, ]

    get_type_DMC <- function(dmc){
      if(nrow(dmc) > 0){
        dmc_grange <- GRanges(seqnames = dmc$chr,
                              IRanges(dmc$end, dmc$end),
                              strand = dmc$strand)
        dmc_type <- list()
        for(i in type){
          annot_grange <- GRanges(seqnames = annot$chr[annot$type == i],
                                  IRanges(annot$start[annot$type == i],
                                          annot$end[annot$type == i]),
                                  strand = annot$strand[annot$type == i])
          subset_type  <- suppressWarnings(subsetByOverlaps(dmc_grange, annot_grange))
          dmc_type[[i]] <- paste0(seqnames(subset_type), ".", start(subset_type))
        }
        dmc_type[["other"]] <- paste0(dmc$chr, ".", dmc$end)[!(paste0(dmc$chr, ".", dmc$end) %in% unlist(dmc_type))]
        dmc_type <- dmc_type[dmc_type != "."]
      }
      return(dmc_type)
    }

    get_number_DMC <- function(dmc_type){
        if(!is.null(dmc_type)){
          number_type <- data.frame(type = as.factor(names(dmc_type)), frequency=sapply(dmc_type, length))
          number_type$type <-factor(number_type$type, levels=number_type$type[order(number_type$frequency, decreasing = TRUE)])
          return(number_type)
        }
      }

    barplot_DMC <- function(number_type, title){
      if(!is.null(number_type)){
        p <- ggplot(number_type, aes(type, frequency)) + geom_bar(stat = "identity") +
          xlab("Type of region") + ylab("Frequency") + ggtitle(title) +
          theme_bw()
        print(p)
      }
    }

    venn_diagram_DMC <- function(number_type, dmc_type, title){
      if(!is.null(number_type)){
	if(nrow(number_type)>1){
          plot.new()
          venn <- venn.diagram(
            x = dmc_type[names(dmc_type) != "other"],
            filename = NULL,
            cex = 1,
            fontface = "bold",
            cat.default.pos = "text",
            cat.cex = 1.5,
            cat.fontfamily = "serif",
            fontfamily = "serif",
            cat.dist = 0.06,
            cat.pos = 0,
            main = title)
          grid.draw(venn)
	}
      }
    }

    if(opt$multifactor){
      #Get number type
      print("Get number types...")
      dmc_type = lapply(1:length(res_DMC), function(i) get_type_DMC(res_DMC[[i]]))
      number_type = lapply(1:length(dmc_type), function(i) get_number_DMC(dmc_type[[i]]))
      #Barplot
      print("Barplot res_DMCs...")
      lapply(1:length(number_type), function(i) barplot_DMC(number_type[[i]], paste("Number of DMC by type of region: ", terms[i], sep="")))
      #Venn diagram
      print("Venn diagram res_DMCs...")
      lapply(1:length(dmc_type), function(i) venn_diagram_DMC(number_type[[i]], dmc_type[[i]], paste("Type of region for DMCs: ", terms[i], sep="")))
    }else{
      #Get number type
      print("Get number type...")
      dmc_type = get_type_DMC(res_DMC)
      number_type = get_number_DMC(dmc_type)
      #Barplot
      print("Barplot res_DMC...")
      barplot_DMC(number_type,"Number of DMC by type of region")
      #Venn diagram
      print("Venn diagram res_DMC...")
      venn_diagram_DMC(number_type, dmc_type, "Type of region for DMCs")
    }

    ### Boxplot
    print("Boxplot rBSobj...")
    cytosines_X_N <- GRanges(seqnames = as.character(seqnames(BSobj)),
                             ranges = IRanges(as.numeric(start(BSobj)),
                                              as.numeric(start(BSobj))),
                             X=getCoverage(BSobj, type = "M"),
                             N=getCoverage(BSobj, type = "Cov"))

    annotGrange <- GRanges(seqnames = annot$chr, IRanges(annot$start, annot$end))

    if(!is.null(opt$comp)){
      #Single factor:
      #proportion by type of region
      propTypeFunc <- function(x){

        temp <- GRanges(seqnames = x$chr[1], IRanges(x$start[1], x$end[1]))

        overlaps <- suppressWarnings(subsetByOverlaps(cytosines_X_N, temp))

        if(length(overlaps) > 0){
          prop1 <- sum(as.data.frame(elementMetadata(overlaps)[, 1:nrow(sample_info)])[, sample_info$sample %in%pool1]) /
            sum(as.data.frame(elementMetadata(overlaps)[, (nrow(sample_info)+1):(2*nrow(sample_info))])[, sample_info$sample %in%pool1])

          prop2 <- sum(as.data.frame(elementMetadata(overlaps)[, 1:nrow(sample_info)])[, sample_info$sample %in%pool2]) /
            sum(as.data.frame(elementMetadata(overlaps)[, (nrow(sample_info)+1):(2*nrow(sample_info))])[, sample_info$sample %in%pool2])

          return(data.frame(proportion = c(prop1, prop2), condition = c(1,2), type = rep(x$type[1], 2)))
        }
      }

      #apply function only on C in a predefine type of region
      overlaps_total <- suppressWarnings(findOverlaps(cytosines_X_N, annotGrange))

      if(length(subjectHits(overlaps_total)) > 0){
        notempty <- unique(as.numeric(subjectHits(overlaps_total)))
        prop_by_type <- adply(annot[notempty,], 1, propTypeFunc, .parallel = opt$parallel)

        #plot
        my_stats <- function(x) {
          res <- quantile(as.numeric(as.character(prop_by_type$proportion[as.numeric(prop_by_type$condition) == x[[1]] &
                                                                            as.character(prop_by_type$type) == x[[2]]])),
                          probs = c(0, 0.25, 0.5, 0.75, 1), na.rm = TRUE) ####ME : added na.rm = TRUE
          names(res) <- c("ymin", "lower", "middle", "upper", "ymax")
          return(res)
        }

        df <- apply(unique(data.frame(prop_by_type$condition, as.character(prop_by_type$type))), 1, my_stats)
        df <- data.frame(unique(data.frame(prop_by_type$condition, as.character(prop_by_type$type))), t(df))
        colnames(df)[1:2] <- c("condition", "type")
        p <- ggplot(df, aes(x = as.factor(condition), ymin = ymin, ymax = ymax, lower = lower,
                            upper = upper, middle = middle, fill = as.factor(condition))) +
          theme_bw() + geom_boxplot(stat = "identity") + facet_grid(.~type) +
          scale_fill_manual(name = "Conditions", values = c("steelblue3", "springgreen3")) +
          ylab("Proportion of methylation") + ggtitle("Boxplot on proportion of methylation
                                                      by type of region and condition") + xlab("Conditions")
        print(p)
      }
    }
  }

  print("TSS...")

  ######## TSS
  dist_to_tss <- NULL
  if(!is.null(opt$tss)){
    prop_after_table <- GRanges(seqnames = as.character(seqnames(BSobj)),
                                ranges = IRanges(as.numeric(start(BSobj)),
                                                 as.numeric(start(BSobj))),
                                prop=getMeth(BSobj, type = "raw"))

    #table with TSS
    tss <- read.table(opt$tss, header = TRUE)
    tssGrange <- GRanges(seqnames = tss$chr, IRanges(tss$tss - 5000, tss$tss + 5000))

    #methylation proportion by distance to TSS
    tssFunc <- function(x){
      temp <- GRanges(seqnames = x$chr[1], IRanges(x$tss[1] - 5000, x$tss[1] + 5000))
      overlaps <- suppressWarnings(subsetByOverlaps(prop_after_table, temp))
      res <- NULL
      if(length(overlaps) > 0){
        for(j in 1:length(overlaps)){

          dist <- ifelse(as.character(x$strand[1]) == "+", start(overlaps)[j] - as.numeric(x["tss"]),
                         as.numeric(x["tss"]) - start(overlaps)[j])
          prop <- as.data.frame(elementMetadata(overlaps))[j, ]

          res <- rbind(res, data.frame(distance = dist, prop))
        }
      }
      return(res)
    }

    #apply the function only on C in TSS windows
    overlaps_total <- suppressWarnings(findOverlaps(prop_after_table, tssGrange))

    if(length(subjectHits(overlaps_total)) > 0){
      notempty <- unique(as.numeric(subjectHits(overlaps_total)))
      dist_to_tss <- adply(tss[notempty,], 1, tssFunc, .parallel = opt$parallel)

      dist_to_tss <- dist_to_tss[, -c(1:3)]
      dist_to_tss_long <- melt(dist_to_tss, id.vars = c("distance"))
      dist_to_tss_long$variable <- sapply(strsplit(as.character(dist_to_tss_long$variable), "[.]"), "[[", 2)

      #plot
      p<-ggplot(dist_to_tss_long,
             aes(x = distance, y = value, colour = variable)) +
        geom_smooth(method="gam", formula = y ~ s(x, k = 50, bs = "cs"), se = FALSE) +
        ylim(c(0,1)) + theme_bw() + guides(color=guide_legend(title="Samples")) +
        xlab("Distance to TSS") + ylab("Smooth proportion of methylation") +
        ggtitle("Smooth proportion of methylation by distance to TSS")
      print(p)
    }
  }

  ###### PCA
  # Create object
  create_obj <- function(path_files, samples, condition){
    count <- match(condition, unique(condition))
    obj=methRead(location=path_files,
                 sample.id=as.list(samples),
                 assembly="Coturnix_japonica_2.0",
                 treatment=count,
                 context="CpG",
                 resolution="base",
                 mincov=opt$min_cov,
                 pipeline=list(fraction=T,chr.col=1,start.col=2,end.col=2,
                               coverage.col=3,strand.col=6,freqC.col=5))
    return(obj)
  }

  get_PCASamples <- function(...){
    # ff <- tempfile()
    # png(filename=ff)
    # res <- PCASamples(..., chunk.size=500000) ###
    res <- PCASamples(...)
    # dev.off()
    # unlink(ff)
    return(res)
  }

  # Plot PCA
  plot_PCA <- function(PCA, metadata, condition){
    p = (autoplot(PCA, data = metadata, colour=condition) + theme_bw()
                  + labs(title="CpG methylation PCA Analysis")
                  + theme(plot.title = element_text(hjust = 0.5)))
    print(p)
  }

  if(opt$multifactor){
    # if multifactor and multiple terms tested
    print("Create an object...")
    obj = lapply(1:length(factors), function(i) create_obj(as.list(bismark_files_path), names, Metadata[,factors[i]]))
    print("Create meth...")
    # Create a methylBase object
    meth = lapply(1:length(factors), function(i) unite(obj[[i]], destrand=FALSE))
    print(paste0(nrow(meth[[1]])," strictly common CpG sites"))
    print("Plot cluster...")
    print(lapply(1:length(factors), function(i) clusterSamples(meth[[i]], dist="correlation", method="ward", plot=TRUE)))
    print("Plot histogram...")
    print(PCASamples(meth[[1]], screeplot=TRUE))
    print("Create PCA...")
    PCA = lapply(1:length(factors), function(i) get_PCASamples(meth[[i]], obj.return=TRUE))
    print("Plot PCA...")
    lapply(1:length(factors), function(i) plot_PCA(PCA[[i]], Metadata, factors[i]))
  }else{
    # single factor
    metadata_pca = merge(sample_info, Metadata, by=1)
    metadata_pca[,1] <- factor(metadata_pca[,1], levels=names)
    metadata_pca = metadata_pca[order(metadata_pca[,1]),]
    print("Create an object...")
    obj = create_obj(as.list(bismark_files_path), names, metadata_pca[, opt$term])
    print("Create meth...")
    # Create a methylBase object
    meth = unite(obj, destrand=FALSE)
    print(paste0(nrow(meth)," strictly common CpG sites"))
    print("Plot cluster...")
    print(clusterSamples(meth, dist="correlation", method="ward", plot=TRUE))
    print("Plot histogram...")
    print(PCASamples(meth, screeplot=TRUE))
    print("Create PCA...")
    PCA = get_PCASamples(meth, obj.return=TRUE)
    print("Plot PCA...")
    plot_PCA(PCA, metadata_pca, opt$term)
    }

  ###### PCA

  dev.off()

  save(PCA, obj, meth,
       file = normalizePath(file.path(opt$out, "PCA.rda"), mustWork = FALSE))

  if(opt$rda){
    print("Save Rdata...")

    if(!is.null(opt$coef)){
      save(diff, diffDMC, dmc_type, dist_to_tss,
           file = normalizePath(file.path(opt$out, "data_for_graph.Rdata"), mustWork = FALSE))
    }else{
      save(diff, diffDMC, dmc_type, prop_by_type, dist_to_tss,
           file = normalizePath(file.path(opt$out, "data_for_graph.Rdata"), mustWork = FALSE))
    }
  }
}
