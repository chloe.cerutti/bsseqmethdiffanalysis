#!/usr/bin/env Rscript

# R version = 3.6.1


### WGBS



############################################################################
### Parametres en input 
############################################################################

rm(list=ls())

packages <- c("optparse",# to read arguments from a command line
              "ggplot2", # nice plots
              "gridExtra", # print plots in output pdf |||| update : better use cowplot
              "data.table" # extension of data.frame for faster development
)


for(package in packages){
  # if package is installed locally, load
  if (!(package %in% rownames(installed.packages()))) {
    print(paste("Need to install the package : ", package, sep = ""))
    stop("Package missing \n", call. = FALSE)
  }
  do.call('require', list(package))
} 


# sapply(packages, function(x){
#   suppressMessages(library(x, character.only = TRUE, quietly = TRUE, warn.conflicts = FALSE))
# })


option_list = list(
  # Compulsory arguments
  
  make_option(c("-o", "--directory"), type = "character", default = NULL, metavar = "character", 
              help = "Path to the output directory"),
  make_option(c("-m", "--metadata"), type = "character", default = "Preprocessed", metavar = "character",
              help = "File of summarized data and experience"),
  make_option(c("-p", "--pathToFiles"), type = "character", default = NULL, metavar = "character",
              help = "Path to the bismark files to be processed "),
  # make_option(c("-s", "--stranded"), type = "logical", default = TRUE, metavar = "logical",
  #             help = "File of summarized data and experience"),
  make_option(c("--process_functions"), type = "character", default = "./BS_process_functions.R", metavar = "character",
              help = "Complete path to the R functions for preprocessing"),
  make_option(c("--mergeStrands"), type = "character", default = TRUE, metavar = "boolean",
              help = "set FALSE to ignore the merging of CpG counts per strand"),
  # Optional arguments
  
  make_option(c("--vcf"), type = "character", default = NULL, metavar = "character",
              help = "SNP file : format VCF (vcf.gz accepted)"),  
  make_option(c("--replicate"), type = "logical", default = TRUE, metavar = "logical",
              help = "Are there replicates in study ?"),
  make_option(c("-x", "--maxCoverage"), type = "integer", default = 1000, metavar = "integer",
              help = "Maximum of coverage depth [default = %default]"),
  make_option(c("-y", "--minCoverage"), type = "integer", default = 5, metavar = "integer",
              help = "Minimum of coverage depth [default = %default]"),
  make_option(c("-z", "--sampleFraction"), type = "double", default = NULL, metavar = "double",
              help = "Minimum fraction of sample per position, between 0 and 1 [default = %default]")
  
  ##Maybe later
  #make_option(c("-c", "--context"), type = "character", default = "CpG", metavar = "character",
  #            help = "CpG or CHG or CHH context [default = %default]"), 
  
)


opt_parser = OptionParser(usage = "\nUSAGE : %prog [options]",
                          option_list = option_list)

opt = parse_args(opt_parser)

source(file = opt$process_functions)



if (is.null(opt$metadata) | is.null(opt$directory)) 
{
  print_help(opt_parser)
  stop("File or directories missing.\n", call. = FALSE)
}



# Path of the output directory
mainDir      <- opt$directory

# Complete path to metadata file
FileMetadata <- opt$metadata

# Path to bismark files
filespath    <- opt$pathToFiles

# Stranded or coverage files ? TRUE for stranded
isStranded   <- opt$stranded

# Complete path to VCF file
vcfFile      <- opt$vcf

# Are there replicates : TRUE / FALSE
isReplicate  <- opt$replicate

# Max depth of coverage allowed for a position 
maxthreshcov <- opt$maxCoverage

# Min depth of coverage allowed for a position
minthreshcov <- opt$minCoverage

# Minimum fraction of sample that share information at a position
sampleFract  <- opt$sampleFraction







############################################################################
### ACTIONS 
############################################################################


### Step 1 : Load files
cat("\n-----Loading datafiles-----\n")

# Load metadata file
bsMetadata <- data.table::fread(file = FileMetadata, header = TRUE, stringsAsFactors=FALSE)
# set keys
setkey(bsMetadata)

# Load bismark's output files
study_whole <- readBScoverage_stranded(BSmetadata = bsMetadata, pathtofiles = filespath, replicate = isReplicate)


### Before filtering
to_plot         <-  study_whole
to_plot         <- to_plot[Cyt != 0 | Thy != 0]
to_plot[, count := Cyt + Thy]
sampleOrder  <- to_plot[, mean(count), by = "label"][order(V1), ]$label
to_plot         <- to_plot[, .N, by = c("label", "count")]
to_plot         <- to_plot[order(label, count), ]
to_plot$step    <- "1- Before filtering"


### Optionnal step 2 : Merging Plus and Minus CpG counts
if(opt$mergeStrands){
  cat("-----Merging Plus and Minus CpG counts-----\n")
  study_whole <- mergePlusMinusPositions(study_DT = study_whole)
  study_whole[, strand := rep("+",nrow(study_whole))]
  study_whole <- study_whole[, c(1,2,3,6,4,5)]
  cat(paste("nb line :", nrow(study_whole), "\n\n", sep = " "))


  ### Step 3 : Remove known snp from the table
  if(is.null(vcfFile)){
    cat("-----Removing SNP positions from study-----\n")
    cat(paste("No VCF file to remove SNP positions", "\n\n"))

  }else{
    cat("-----Removing SNP positions from study-----\n")
    study_whole <- removeSNPwithVCF(VCF_path = vcfFile, study_DT = study_whole)

    cat(paste("nb line :", nrow(study_whole), "\n\n", sep = " "))
  }

}else{

  if(is.null(vcfFile)){
    cat("-----Removing SNP positions from study-----\n")
    cat(paste("No VCF file to remove SNP positions", "\n\n"))

  }else{
    cat("-----Removing SNP positions from study-----\n")
    study_whole <- removeSNPwithVCF_stranded(VCF_path = vcfFile, study_DT = study_whole)

    cat(paste("nb line :", nrow(study_whole), "\n\n", sep = " "))
  }


}

### After removing SNP and strand merging
add2plot      <-  study_whole
add2plot      <- add2plot[Cyt != 0 | Thy != 0]
add2plot[, count := Cyt + Thy]
add2plot      <- add2plot[, .N, by = c("label", "count")]
add2plot      <- add2plot[order(label, count), ]
add2plot$step <- "2 -Strand merging & SNP filtering"
to_plot       <- rbind(to_plot, add2plot)


### Step 4 : Make List of position to filter with low depth and sampleFract parameters

cat("-----get minDepth-----\n")

pos_to_remove_minDepth <- BSFilterMinDepth(study_DT = study_whole, minDepth = minthreshcov, sampleFraction = sampleFract)

cat(paste("nb line :", nrow(study_whole), "\n\n", sep = " "))



### Step 5 : Make List of position with high depth

cat("-----get maxDepth-----\n")

pos_to_remove_maxDepth <- BSFilterMaxDepth(study_DT = study_whole, maxDepth = maxthreshcov)

cat(paste("nb line :", nrow(study_whole), "\n\n", sep = " "))


#saveRDS(study_whole, file.path(mainDir, "study_whole_bfr_covnorm.RDS"))

### Step 6 : Median normalisation

cat("-----normalisation-----\n")

study_whole <- BSCoverageNorm(study_DT = study_whole)


#saveRDS(study_whole, file.path(mainDir, "study_whole_aft_covnorm.RDS"))

cat(paste("nb line :", nrow(study_whole), "\n\n", sep = " "))

### After normalisation
add2plot      <-  study_whole
add2plot      <- add2plot[Cyt != 0 | Thy != 0]
add2plot[, count := Cyt + Thy]
add2plot      <- add2plot[, .N, by = c("label", "count")]
add2plot      <- add2plot[order(label, count), ]
add2plot$step <- "3- After Normalisation"
to_plot       <- rbind(to_plot, add2plot)



### Step 7 : Remove positions listed in 3 and 4
cat("-----remove minDepth-----\n")
setkey(study_whole, start, chr)
setkey(pos_to_remove_minDepth, start, chr)
cat(paste("removing :", nrow(study_whole[pos_to_remove_minDepth]), "\n\n", sep = " "))
study_whole <- study_whole[!pos_to_remove_minDepth]


##After removing minDepth
add2plot   <-  study_whole
add2plot   <- add2plot[Cyt != 0 | Thy != 0]
add2plot[, count := Cyt + Thy]
add2plot   <- add2plot[, .N, by = c("label", "count")]
add2plot   <- add2plot[order(label, count), ]
add2plot$step <- "4 - After MinDepth"
to_plot    <- rbind(to_plot, add2plot)



cat("-----remove maxDepth-----\n")
setkey(pos_to_remove_maxDepth, start, chr)
cat(paste("removing :", nrow(study_whole[pos_to_remove_maxDepth]), "\n\n", sep = " "))
study_whole <- study_whole[!pos_to_remove_maxDepth]


##After removing maxDepth
add2plot <-  study_whole
add2plot  <- add2plot[Cyt != 0 | Thy != 0]
add2plot[, count := Cyt + Thy]
add2plot <- add2plot[, .N, by = c("label", "count")]
add2plot <- add2plot[order(label, count), ]
add2plot$step <- "5 - After MaxDepth"
to_plot <- rbind(to_plot, add2plot)


## PLOT HISTOGRAM
dir.create(file.path(mainDir,"plots"), recursive = TRUE)

p <- ggplot(to_plot, aes(x = factor(label, levels = sampleOrder), y = N, fill = count)) +
	geom_col() +
	scale_fill_viridis_c(limits = c(1, 100), oob = scales::squish) +
	labs(
		title = "CpG filtering",
		x = "Sample",
		y = "Number of CpG sites",
		fill = "Coverage"
	) +
	scale_y_continuous(labels = scales::comma) +
	theme_bw(base_size = 14) +
	theme(axis.text.x = element_text(angle = 90, hjust = 1, size = 8)) +
	facet_wrap(~step, ncol = 4)

ggsave(file.path(mainDir,"plots", "all_thresholding_steps.png"), p, width = 18, height = 10)


### Step 8 : Write tables in BSseq

cat("-----Generating and writing tables in BSseq format-----\n")

### Split study_whole in a file per sample
listDT <- makeBStablePerSample(study_whole)


cat(paste("nb of table in list :", length(listDT), "\n"))

### Write output to table
writeBStables(listDT, mainDir)

